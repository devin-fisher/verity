include "lib-indy.conf"
include "metrics.conf"
include "resource-usage-rule.conf"

verity {

  endpoint = {
    # provide ip address which is reachable by outside world
    host = "localhost"

    # this would be same as http.port
    port = ${verity.http.port}
  }

  services {

    # this is all push notification related configuration, only required if configuring consumer agent service
    # keep default value of most except 'default-logo-url'
    push-notif-service = {
      general-msg-title-template = "Hi #{targetName}"
      general-new-msg-body-template = "#{senderName} sent you #{msgType}"
      error-resp-msg-body-template = "#{senderName} responded with error (detail: uid -> #{uid}, msg type -> #{msgType})"

      default-sender-name = "Remote connection"

      # if any error response is reported from remote agent (enterprise agent), it is being notified to user via push notification
      # that time, consumer agent uses this logo url which app renders it
      # this is also used if at all consumer agent receives a msg from remote agent without its logo url
      default-logo-url = "https://robohash.org/234"

      fcm {
        host = "fcm.googleapis.com"
        path = "/fcm/send"

        # this is firebase push notification serve key which you might/would have configured
        # so that CAS to be able to send push notifications to connect.me app
        key = "AAAAMz4rOdM:APA91bE-TPbHE12RmnT-34HSQjDoUNqBGCiSeX2HoB-JNmJgBjNc_JZ4Q3Iu0AefRd6eN13ly5CX95QcFS3vNZ7Ba2pDoQUqOowJB6QJQPUO_nqkosP8XKmRyFgmEJGJpNLqU4fmSmKP"
      }
    }
  }

  app-state-manager {
    state {
      initializing {
        max-retry-count = 10
        max-retry-duration = 240
      }
      draining {
        delay-before-leave = 1
        delay-between-status-checks = 0.1
        max-status-check-count = 20
      }
    }
  }


  user-agent-pairwise-actor {
    scheduled-job {
      initial-delay-in-seconds = 5
      interval-in-seconds = 30
    }
  }

  item-container {

    scheduled-job {
      initial-delay-in-seconds = 5
      interval-in-seconds = 60
    }

    migration {
      chunk-size = 5
    }

  }

  libvcx {
    enable_test_mode = true
    institution_name = "Acme"
  }
}

verity {
  rest-api {
    enabled = false
  }
}

# NOTE: akka.test section is to configure client side timeouts
akka {
  test {
    single-expect-default = 5s
  }

  sharding-region-name {
    user-agent = "UserAgent"
    user-agent-pairwise = "UserAgentPairwise"
  }

}

# NOTE: verity.timeout section is to configure server side timeouts
verity {
  test {
    http-route-timeout-in-seconds = 20
  }
  timeout {
    general-actor-ask-timeout-in-seconds = 10
    general-actor-ref-resolve-timeout-in-seconds = 10
  }
}

verity {
  # Documentation in reference.conf
  retention-policy {
    default {
      undefined-fallback {
        expire-after-days = "360 days"
      }
    }
  }
}

verity.blob-store {
  bucket-name = "blob-store"
  storage-service = "com.evernym.verity.storage_services.leveldb.LeveldbAPI"
  local-store-path = "/tmp/verity/leveldb"
}

verity.provisioning {
  sponsors = [
    {
      name = "test-sponsor"
      id = "test-sponsorabc123"
      keys = [{"verKey":"test-sponsor456def8910"}]
      endpoint = "test-sponsorgoogle.com"
      active = false
    },
    {
      name = "inactive-sponsor-name"
      id = "inactive-sponsor-id"
      keys = [{"verKey": "GJ1SzoWzavQYfNL9XkaJdrQejfztN4XqdsiV4ct3LXKL"}]
      endpoint = "localhost:3456/json-msg"
      active = false
    },
    {
      name = "evernym-test-sponsor"
      id = "evernym-test-sponsorabc123"
      keys = [{"verKey": "GJ1SzoWzavQYfNL9XkaJdrQejfztN4XqdsiV4ct3LXKL"}]
      endpoint = "localhost:3456/json-msg"
      active = true
    }
  ]
  sponsor-required = false
  token-window = 10 minute
  cache-used-tokens = true
}

verity {
  agent {
    authentication {

      # determines if this feature is by default available or not
      enabled: true

      # map of domainIds and their authorized keys
      keys {
        # provided keys will be available to agent belonging to given DID as key
        domain-id-1: ["key1", "key2"]
      }
    }
  }
}

verity {
  services {
    push-notif-service = {
      # mock cloud messaging, by default turned off, it is only used during integration test
      mcm {
        enabled: true
      }
    }
  }
}


verity {
  services.push-notif-service.enabled = true

  url-mapper-api {
    enabled = true
  }
}

verity.services {
  push-notif-service {
    # mock cloud messaging
    mcm {
      send-messages-to-endpoint = true
    }
  }
}

verity {
  cache {
    agency-detail.expiration-time-in-seconds = 0
  }
}

verity {
  agent {
    actor-state-cleanup {
      enabled = false
    }
    migrate-thread-contexts {
      enabled = false
    }
  }
}

kamon {
  # increase tick interval for tests to reduce waiting time for metrics to be reported
  metric.tick-interval = 1 second
}

verity {
  salt {
    # salt which is mixed to generate secure hashed wallet name
    wallet-name = "5k4k4k3k4k5l"
    # salt which is mixed to get symmetric key to encrypt/decrypt wallet
    wallet-encryption = "fGCPX33373n7hImz4T5y"
    # salt which is mixed to get symmetric key to encrypt/decrypt database events
    event-encryption = "qo9V010GhLAqOpF0jMhKlOuJnM34G6NHkBePojHgh1HgNg872k"
  }
}

verity {
  secret {
    # secret used to get symmetric key to encrypt/decrypt routing agent actor db events
    routing-agent = "4k3kejd845k4k3j"
    # secret used to get symmetric key to encrypt/decrypt url-mapper actor db events
    url-mapper-actor = "klsd89894kdsjisdji4"
    # secret used to get symmetric key to encrypt/decrypt key-value mapper actor db events
    key-value-mapper-actor = "krkifcjk4io5k4k4kl"
    # secret used to get symmetric key to encrypt/decrypt token-to-actor-item-mapper actor db events
    token-to-actor-item-mapper-actor = "djkdu4jkeidui4"
    # secret used to get symmetric key to encrypt/decrypt user-warning-status-mngr actor db events
    user-warning-status-mngr = "jh3jqtcri1hdj"
    # secret used to get symmetric key to encrypt/decrypt user-blocking-status-mngr actor db events
    user-blocking-status-mngr = "ki4krudsj2iek"
    # secret used to get symmetric key to encrypt/decrypt resource usage tracker actor db events
    resource-usage-tracker = "dsydskriclrt"
  }
}

verity {
  services {
    url-mapper-service {
      # right now url-mapper-service is hosted in Consumer Agent Service (aka 'cas')
      # if you are configuring consumer agent service itself then host=localhost, port = http.port
      # if you are configuring enterprise agent service, then host=cas's ip address, port = cas's.http.port
      # keep default value for 'path-prefix', unless you know why you are changing it
      endpoint {
        host = "localhost"
        port = 9000
        path-prefix = "agency/url-mapper"
      }

      msg-template {
        # template by which shortened url is build
        connect-me-mapped-url-template = "https://connectme.app.link?t=#{token}"
      }
    }
  }
}

verity {
  services {
    sms-service {
      # if you know all configurations value related to external sms services (bandwidth and twilio)
      # and want sms to be sent by local verity only then, below config should be "Y",
      # else set it to "N", and then, you'll have to properly configure below mentioned "endpoint" property
      send-via-local-agency = "Y"
      # if above configuration 'send-via-local-agency' is set to 'N',
      # then, only the below 'endpoint' configuration matters else you can ignore it
      # this endpoint should be of the service which is hosting sms service
      # consumer service agent will host sms service for sure
      endpoint {
        # ip address of the the target service which serves sms sending service api calls
        # if 'send-via-local-agency' is set to 'Y', then you can remove the word 'TODO'
        # from below value and then it won't prompt you to put proper value for that configuration
        host = "localhost"
        # http port where the target service which serves sms sending service api calls
        # mostly it should be http.port of that target service
        port = 9000
        # keep it with default value (agency/sms)
        path-prefix = "agency/sms"
      }

      # below mentioned ip addresses only would be allowed to use sms-service if hosted by this verity
      # if you are expecting one or many enterprise agents to use your sms sending service
      # include their ip addresses in this configuration
      allowed-client-ip-addresses = ["127.0.0.1/32"]

      external-services {
        # we are using two SMS service to send sms, BW = Bandwidth and TW = Twilio
        # below order defines in which order verity should try which service to send sms
        # if first service respond with any error, then only second service is tried
        preferred-order = []
      }
    }
  }
}
